# Aviation Waypoints

Provide the crawler and a function to get a list of waypoints. The data is crawled from opennav.com

To use get the list of waypoints, please provide a list of countries (short name or fullname, case insensitive) to function get_waypoint.

For example:
```python
# get the list of waypoints of Vietnam, Singapore, Malaysia, Indonesia
waypoints = get_waypoints(['vn', 'sg', 'my', 'id'])
# the following call returns similar result
waypoints = get_waypoints(['vietnam', 'singapore', 'my', 'id'])
```