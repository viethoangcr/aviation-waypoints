import requests
import logging
import pandas as pd
import numpy as np

from path import Path
from typing import Optional, List
from pygeodesy.ellipsoidalKarney import LatLon

logger = logging.getLogger(__name__)

COUNTRIES = pd.read_csv('./data/countries.csv', sep='\t')
BASE_URL = r"https://opennav.com/waypoint/"
SAVED_DATA_LOCATION = Path("./data/")
WAYPOINT_FILENAME = "waypoints_{country_code}.csv"


def _format_table(df: pd.DataFrame) -> pd.DataFrame:
    res = df.dropna(axis=0, how="all")
    res.dropna(axis=1, how="all", inplace=True)
    lats, longs = [], []
    for lat, lon in zip(res["LATITUDE"].values, res["LONGITUDE"].values):
        try:
            ll = LatLon(lat, lon)
            lats.append(ll.lat)
            longs.append(ll.lon)
        except Exception as e:
            logger.exception(e)
            lats.append(np.nan)
            longs.append(np.nan)

    res["LATITUDE"] = lats
    res["LONGITUDE"] = longs
    return res


def _get_country(countries: Optional[List[str]] = None):
    if countries is None:
        countries = COUNTRIES
    else:
        countries = [s.lower() for s in countries]
        idx = (COUNTRIES["Alpha-2 code"].str.lower().isin(countries)) | \
              (COUNTRIES["Alpha-3 code"].str.lower().isin(countries)) | \
              (COUNTRIES["Country"].str.lower().isin(countries))
        countries: pd.DataFrame = COUNTRIES[idx]
    return countries


def update_waypoints(countries: Optional[List[str]] = None):
    countries = _get_country(countries)

    for code, name in countries[["Alpha-2 code", "Country"]].itertuples(index=False, name=None):
        logger.info(f"Updating waypoint list of {name}")
        try:
            dfs = pd.read_html(BASE_URL + code, header=0, match='LONGITUDE')
            if dfs and dfs[0].shape[0] > 1:
                df = _format_table(dfs[0])
                df.to_csv(SAVED_DATA_LOCATION / WAYPOINT_FILENAME.format(country_code=code), index=False)
                logger.info(f"Updated successfully with {df.shape[0]} waypoints for {name}")
            else:
                logger.warning(f"Could not find waypoint table for {name}")
        except ValueError as ve:
            logger.info(f"No waypoint tables found for {name}")


def get_waypoints(countries: Optional[List[str]] = None) -> pd.DataFrame:
    countries = _get_country(countries)

    need_to_update_list = []
    for code in countries["Alpha-2 code"].values:
        filepath = SAVED_DATA_LOCATION / WAYPOINT_FILENAME.format(country_code=code)
        if not filepath.exists():
            need_to_update_list.append(code)

    update_waypoints(need_to_update_list)

    dfs = []
    for code, name in countries[["Alpha-2 code", "Country"]].itertuples(index=False, name=None):
        filepath = SAVED_DATA_LOCATION / WAYPOINT_FILENAME.format(country_code=code)
        if filepath.exists():
            df = pd.read_csv(filepath)
            df['Country'] = code
            df['Country Fullname'] = name
            dfs.append(df)

    return pd.concat(dfs, ignore_index=True)
